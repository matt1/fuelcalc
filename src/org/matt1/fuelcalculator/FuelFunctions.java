package org.matt1.fuelcalculator;

import java.text.NumberFormat;

public class FuelFunctions {

	public static FuelFunctions instance = new FuelFunctions();

	public static FuelFunctions getInstance() {
		return instance;
	}

	float LitresInGallon = 4.54609188f;
	float GallonsInLitre = 0.219969157f;
	float MilesInKilometer = 0.621371192f;
	float KilometersInMiles = 1.609344f;
	static NumberFormat formater = NumberFormat.getNumberInstance();
	static NumberFormat currencyFormater = NumberFormat.getCurrencyInstance();
	static {
		currencyFormater.setMaximumFractionDigits(2);
		formater.setMaximumFractionDigits(2);
	}
	

	public NumberFormat getCurrencyFormater() {
		return currencyFormater;
	}
	
	public NumberFormat getFormater() {
		return formater;
	}
	
	public float Gallons(float FuelCost, float Spend) {

		float Litres = (Spend / (FuelCost / 100));
		return (Litres * GallonsInLitre);

	}

	public float RealMPG(float FuelCost, float Spend, float Miles) {

		float g = Gallons(FuelCost, Spend);
		return (Miles / g);

	}

	public float RequiredFuel(float Miles, float MPG) {

		return (Miles / MPG);

	}

	public float JourneyCost(float Miles, float MPG, float FuelCost) {

		float GallonsUsed;
		GallonsUsed = (Miles / MPG);
		return ((GallonsUsed * LitresInGallon) * FuelCost) / 100;

	}

	public float ToLitresPer100KM(float MPG) {

		return (100 / ((MPG * KilometersInMiles) * GallonsInLitre));

	}

	public float ToMPG(float LitresPer100KM) {
		return ((100 / (LitresPer100KM * GallonsInLitre)) * MilesInKilometer);
	}

	public float ToMiles(float KM) {
		return (KM * MilesInKilometer);
	}

	public float ToKM(float Miles) {
		return (Miles * KilometersInMiles);

	}
	
	public float ToLitres(float Gallons) {
		return Gallons * LitresInGallon;
	}
}
