package org.matt1.fuelcalculator;

import java.text.NumberFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class JourneyCost extends Fragment {

	private EditText miles;
	private EditText cost;
	private EditText mpg;
	private TextView result;
	
	@Override
    public View onCreateView(LayoutInflater inflater, 
    		ViewGroup container,Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.journey_cost, container, false);
        
        // setup change listeners
        
        miles = (EditText) rootView.findViewById(R.id.journeyCostMiles);
        cost = (EditText) rootView.findViewById(R.id.journeyCostCost);
        mpg = (EditText) rootView.findViewById(R.id.journeyCostMPG);
        result = (TextView) rootView.findViewById(R.id.journeyCostResult);
        
        TextWatcher changer = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				calculate();
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		};
        
        miles.addTextChangedListener(changer);
        cost.addTextChangedListener(changer);
        mpg.addTextChangedListener(changer);
        
        
        return rootView;
	
	}
	
	private void calculate() {
		String mileValue = miles.getText().toString();
		String mpgValue = mpg.getText().toString();
		String costValue = cost.getText().toString();
		
		float mileFloat, mpgFloat, costFloat;
		
		try {
			mileFloat = Float.parseFloat(mileValue);
			mpgFloat = Float.parseFloat(mpgValue);
			costFloat = Float.parseFloat(costValue);
			
			NumberFormat f = FuelFunctions.getInstance().getCurrencyFormater();
			float totalcost = FuelFunctions.getInstance().JourneyCost(mileFloat, mpgFloat, costFloat);
			result.setText(f.format(totalcost));
			
		} catch (NumberFormatException nfe) {
			result.setText("");
		}

	}
	
	
	
}
