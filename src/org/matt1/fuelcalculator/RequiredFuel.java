package org.matt1.fuelcalculator;

import java.text.NumberFormat;

import org.matt1.fuelcalculator.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class RequiredFuel extends Fragment {

	private EditText miles;
	private EditText mpg;
	private TextView result;
	private TextView resultUnits;
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, 
    		ViewGroup container,Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.required_fuel, container, false);
// setup change listeners
        
        miles = (EditText) rootView.findViewById(R.id.requiredFuelDistance);
        mpg = (EditText) rootView.findViewById(R.id.requiredFuelMPG);
        result = (TextView) rootView.findViewById(R.id.requiredFuelResult);
        resultUnits = (TextView) rootView.findViewById(R.id.requiredFuelLitres);
        
        TextWatcher changer = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				calculate();
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		};
        
        miles.addTextChangedListener(changer);
        mpg.addTextChangedListener(changer);
        
        
        return rootView;
	
	}
	
	private void calculate() {
		String mileValue = miles.getText().toString();
		String mpgValue = mpg.getText().toString();
		
		float mileFloat, mpgFloat;
		
		try {
			mileFloat = Float.parseFloat(mileValue);
			mpgFloat = Float.parseFloat(mpgValue);
			
			float totalgallons = FuelFunctions.getInstance().RequiredFuel(mileFloat, mpgFloat);
			float totalLitres = FuelFunctions.getInstance().ToLitres(totalgallons);
			NumberFormat f = FuelFunctions.getInstance().getFormater();
			result.setText(f.format(totalLitres));
			resultUnits.setText("litres");
			
		} catch (NumberFormatException nfe) {
			result.setText("");
			resultUnits.setText("");
		}

	}
	
}
