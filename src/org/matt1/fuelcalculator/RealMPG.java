package org.matt1.fuelcalculator;

import java.text.NumberFormat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class RealMPG extends Fragment {

	private EditText totalCost;
	private EditText fuelCost;
	private EditText distance;
	private TextView result;
	private TextView resultMpg;
	
	@Override
    public View onCreateView(LayoutInflater inflater, 
    		ViewGroup container,Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.real_mpg, container, false);
        
        // setup change listeners
        
        fuelCost = (EditText) rootView.findViewById(R.id.realMpgCost);
        totalCost = (EditText) rootView.findViewById(R.id.realMpgTotalCost);
        distance = (EditText) rootView.findViewById(R.id.realMpgDistance);
        result = (TextView) rootView.findViewById(R.id.realMpgResult);
        resultMpg = (TextView) rootView.findViewById(R.id.realMpgMpg);
        
        TextWatcher changer = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				calculate();
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {}
		};
        
        fuelCost.addTextChangedListener(changer);
        totalCost.addTextChangedListener(changer);
        distance.addTextChangedListener(changer);
        
        
        return rootView;
	
	}
	
	private void calculate() {
		String fuelCostValue = fuelCost.getText().toString();
		String totalCostValue = totalCost.getText().toString();
		String distanceValue = distance.getText().toString();
		
		try {
			float fuelCostFloat = Float.parseFloat(fuelCostValue);
			float totalCostFloat = Float.parseFloat(totalCostValue);
			float distanceFloat = Float.parseFloat(distanceValue);
			
			NumberFormat f = FuelFunctions.getInstance().getFormater();
			float totalcost = FuelFunctions.getInstance().RealMPG(fuelCostFloat, totalCostFloat, distanceFloat);
			result.setText(f.format(totalcost));
			resultMpg.setText("MPG");
		} catch (NumberFormatException nfe) {
			result.setText("");
			resultMpg.setText("");
		}

	}
	
	
	
}
